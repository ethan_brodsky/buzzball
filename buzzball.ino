// buzzball
// Ethan Brodsky

// Copyright 2019 by Ethan Brodsky.  All rights reserved.

// Certain rights are available to EAA members in accordance with the 2017-2018 and 2018-2019 Innovation Prize rules
// see https://www.eaa.org/~/media/files/eaa/advocacy/10-19-17-eaa-2017-2018-fip-rule%20-%20final%20171019.pdf and 
//     https://eaa.org/-/media/Files/EAA/Advocacy/EAA-2018-2019-FIP-Rules-FINAL-190319.ashx for details


#pragma GCC diagnostic ignored "-Wwrite-strings"

//#define DEBUG_OUTPUT  // Turning this on makes everything too slow to work properly
#define DO_LOGGING

//#define DEBUG_SPIN_LATCH // used for testing spin mode - do not enable for in-flight use

#include <Wire.h>

#include <Adafruit_MotorShield.h>

#define USE_LSM303

#ifdef USE_LSM303
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#endif

#include "NineAxesMotion.h" 
#include <utility/BNO055.h>

NineAxesMotion imu;   

// for datalogger
#include <SdFat.h>
#include "RTClib.h"

#define SAMPLE_PERIOD 2500       // us      // 10000 for BNO055
#define BNO055_PERIOD 10000      // us
#define TICK_PERIOD   100000     // us

#define BNO055_DECIMATION_RATE (BNO055_PERIOD/SAMPLE_PERIOD)

#define TICKS_PER_DISPLAY 5
#define STARTUP_BUZZ_TICKS 9

#define SAMPLES_PER_TICK (TICK_PERIOD / SAMPLE_PERIOD)

#define ACCEL_ZERO 0.00  // Zero must be adjusted for installation 
#define ACCEL_GAIN -0.86 // Gain should remain fixed across all aircraft (not sure if it is coincidental that it is sqrt(3)/2)

#define BALL_THRESHOLD  0.375
#define BALL_HYSTERESIS 0.050

#define SPIN_THRESHOLD 100 // degrees/second

#define SPIN_MIN_TIME 1.0 // seconds
#define SPIN_MIN_TICKS ((int)((SPIN_MIN_TIME*1e6/TICK_PERIOD)))

#define TAU_FAST 0.25
#define TAU_SLOW 2.00

#define TAU_HEADING 0.10

#define BALL_THRESHOLD_FAST 0.375
#define BALL_THRESHOLD_SLOW 0.300

// Create the motor shield object with the default I2C address
Adafruit_MotorShield MotorDriver = Adafruit_MotorShield(0x60);   

// Create motor objects
Adafruit_DCMotor *motor_L = MotorDriver.getMotor(3);
Adafruit_DCMotor *motor_R = MotorDriver.getMotor(4);

long  sample  = 0;
long  tick    = 0;
short rawtick = 0;

bool buzz_L = 0;
bool buzz_R = 0;

double ay_fast = 0;
double ay_slow = 0;

int    spin_counter = 0;

double k_fast;
double k_slow;
double k_heading;

#define HEADING_BUF_LENGTH 3
float heading_buf[HEADING_BUF_LENGTH];

long time_lasttick = 0;

#ifdef DEBUG_SPIN_LATCH
bool spin_latch_R = false;
bool spin_latch_L = false;
#endif

#ifdef USE_LSM303
// Assign unique IDs to each sensor
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(12345);
#endif

RTC_PCF8523 RTC;

#ifdef DO_LOGGING  
SdFat  sd;
SdFile logfile;
#endif

typedef struct
  {
    float accelX;
    float accelY;
    float accelZ;
    float heading;
    float pitch;
    float roll;
    int   cal_accel;
    int   cal_mag;
    int   cal_gyro;
    int   cal_sys;
  } IMU_STATE;

void error_halt()
  {
   // shut off motors
    motor_L->run(RELEASE);
    motor_R->run(RELEASE);

   // flash LED slowly
    pinMode(LED_BUILTIN, OUTPUT);
    for (;;)
      {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(1000);
        digitalWrite(LED_BUILTIN, LOW);
        delay(1000);
      }
  }

void motor_activate(Adafruit_DCMotor *motor, bool motor_activate)
  {
    motor->setSpeed(motor_activate ? 255 : 0);
  }

void callback_datetime(uint16_t* datestamp, uint16_t* timestamp)
  {
    DateTime now = RTC.now();

    *datestamp = FAT_DATE( now.year(), now.month(), now.day() );
    *timestamp = FAT_TIME( now.hour(), now.minute(), now.second() );
  }

#ifdef DO_LOGGING  
void setupLogging()
  {
    if (!RTC.begin())
      {
        Serial.println(F("Couldn't initialize RTC"));
        error_halt();
      }

    const int chipSelect = 10;
    pinMode(chipSelect, OUTPUT);

    if (!sd.begin(chipSelect, SD_SCK_MHZ(50)) )
      {
        Serial.println(F("Couldn't open SD card"));
        error_halt();
      }

   // setup callback so that file timestamps are correct
    SdFile::dateTimeCallback(callback_datetime);

    char filename[64];

    DateTime now = RTC.now();
    sprintf(filename, "log_%02d%02d%02d_%02d%02d%02d.txt", now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second() );

    if (!logfile.open(filename, O_CREAT | O_WRITE | O_EXCL))
      {
        Serial.println(F("Couldn't create file"));
        error_halt();
      }

    Serial.print(F("Opened logfile: '"));
    Serial.print(filename);
    Serial.print(F("' \n"));

    char buf[64];
    sprintf(buf, "Log Started: %04d-%02d-%02d %02d:%02d:%02d.%03d  ", now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second(), millis() % 1000);

    logfile.println(buf);
    logfile.sync();

   // disable callback to avoid continuously updating file timestamp during logging
    SdFile::dateTimeCallbackCancel(); 
  }

int flushcount = 0;

void writeLog(long tick, float ticktime_ms, IMU_STATE imuState, float ay_raw, float ballpos_fast, float ballpos_slow, bool buzz_L, bool buzz_R, float heading, float heading_rate, bool spin_L, bool spin_R)
  {
    DateTime now = RTC.now();
  
    char buf[64];
    sprintf(buf, "%02d:%02d:%02d.%03d  ", now.hour(), now.minute(), now.second(), millis() % 1000);
  
    logfile.print(buf);
  
    logfile.print(tick);
    logfile.print(" (");
    logfile.print(ticktime_ms);
    logfile.print(")   aX/aY/aZ: ");
    logfile.print(imuState.accelX);
    logfile.print(" ");
    logfile.print(imuState.accelY);
    logfile.print(" ");
    logfile.print(imuState.accelZ);

    logfile.print(" H/P/R: ");
    logfile.print(imuState.heading);
    logfile.print(" ");
    logfile.print(imuState.pitch);
    logfile.print(" ");
    logfile.print(imuState.roll);

    logfile.print(" CalStatus=");
    logfile.print(imuState.cal_accel);
    logfile.print(imuState.cal_mag);
    logfile.print(imuState.cal_gyro);
    logfile.print(imuState.cal_sys);

    logfile.print(" LSM303 aY: ");
    logfile.print(ay_raw);
    
    logfile.print(" (ball F=");
    logfile.print(ballpos_fast);
    logfile.print(" S=");
    logfile.print(ballpos_slow);
    logfile.print(") L");
  
    logfile.print(buzz_L);
    logfile.print(" R");
    logfile.print(buzz_R);

    logfile.print("   H: " );
    logfile.print(heading);
    logfile.print(" ");
    logfile.print(heading_rate);
    logfile.print(" sL ");
    logfile.print(spin_L);
    logfile.print("  sR ");
    logfile.print(spin_R);
    
    logfile.println();
  
    // flush on regular intervals
    if (flushcount-- <= 0)
      {
        // turn off motors momentarily during flush (eliminate possibility of erroneous indication during long flush)
        motor_activate(motor_L, false);
        motor_activate(motor_R, false);
    
        logfile.flush();
    
        flushcount = 100;
      }
  }
#endif  

void setup(void)
  {
    Serial.begin(115200);
    Serial.println("");
    Serial.println("Buzz Ball");
    Serial.println("");

#ifdef DO_LOGGING  
    setupLogging();
#endif

#ifdef USE_LSM303  
    if (!accel.begin())
      {
        Serial.println(F("Error initializing accelerometer"));
        error_halt();
      }
  
    if (!mag.begin())
      {
        Serial.println(F("Error initializing magnetometer"));
        error_halt();
      }
#endif      

    I2C.begin();                             // Initialize I2C communication to the let the library communicate with the sensor.
    imu.initSensor();                        // The I2C Address can be changed here inside this function in the library
    bno055_set_axis_remap_value(REMAP_X_Y);  
    imu.setOperationMode(OPERATION_MODE_NDOF);   
    imu.setUpdateMode(MANUAL);               // Default is AUTO. MANUAL requires calling the relevant update functions prior to calling the read functions

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    MotorDriver.begin();

    motor_L->run(FORWARD);
    motor_R->run(FORWARD);

    motor_activate(motor_L, false);
    motor_activate(motor_R, false);

    // set up filters
    k_fast = exp(-SAMPLE_PERIOD / 1.0e6 / TAU_FAST);
    k_slow = exp(-SAMPLE_PERIOD / 1.0e6 / TAU_SLOW);
    k_heading = exp(-SAMPLE_PERIOD / 1.0e6 / TAU_HEADING);

#ifdef DO_LOGGING  
    // Output configuration
    logfile.println(SAMPLE_PERIOD);
    logfile.println(TICK_PERIOD);
    logfile.println(TAU_FAST);
    logfile.println(TAU_SLOW);
    logfile.println(TAU_HEADING);
    logfile.println(k_fast);
    logfile.println(k_slow);
    logfile.println(k_heading);
    logfile.println(ACCEL_ZERO);
    logfile.println(ACCEL_GAIN);
    logfile.println(BALL_THRESHOLD_FAST);
    logfile.println(BALL_THRESHOLD_SLOW);
    logfile.println(SPIN_THRESHOLD);
    logfile.println(F("----------------"));
#endif    
  }

float accelX = 0;
float accelY = 0;
float accelZ = 0;

void loop(void)
  {
    long time_sample_start = micros();

#ifdef USE_LSM303  
    // Read accelerometer
    sensors_event_t eventAccel;
    accel.getEvent(&eventAccel);

    // Read magnetometer
    sensors_event_t eventMag;
    mag.getEvent(&eventMag);
#endif

    if ((rawtick++ % BNO055_DECIMATION_RATE) == 0)
      {
        imu.updateAccel(); 
        accelX  = imu.readAccelerometer(X_AXIS);   
        accelY  = imu.readAccelerometer(Y_AXIS);   
        accelZ  = imu.readAccelerometer(Z_AXIS);   
      }

// xxxx this code is for BNO055-driven algorithm
//    double ay_raw = accelY;
    double ay_raw = eventAccel.acceleration.y;   // get acceleration from LSM303 intead of BNO055
    
    sample++;
  
    ay_fast = k_fast * ay_fast + (1 - k_fast) * ay_raw;
    ay_slow = k_slow * ay_slow + (1 - k_slow) * ay_raw;

    if (sample >= SAMPLES_PER_TICK)
      {
        sample = 0;
    
        long time_thistick = micros();
        long ticktime = time_thistick - time_lasttick;
        time_lasttick = time_thistick;

        imu.updateEuler();        
        imu.updateCalibStatus();  

        IMU_STATE imuState;

       // Acceleration
        imuState.accelX  = accelX;   // + = FORWARD, - = AFT, 
        imuState.accelY  = accelY;   // + = RIGHT,   - = LEFT 
        imuState.accelZ  = accelZ;   // + = UP,      - = DOWN (upright gravity is +)

       // Heading, pitch, and bank angles 
        imuState.heading = (imu.readEulerHeading() + 0);
        if (imuState.heading > 360)
          imuState.heading -= 360;
    
       // ROLL AND PITCH ARE SWAPPED (due to axis remapping)
        imuState.roll    = +imu.readEulerPitch();     // - = BANK LEFT, + = BANK RIGHT
        imuState.pitch   = +imu.readEulerRoll();      // - = NOSE DOWN, + = NOSE UP
    
        // see discussion at https://forums.adafruit.com/viewtopic.php?f=19&t=88600 
        // and https://forums.adafruit.com/viewtopic.php?f=19&t=91723&p=462337#p462337 
        // regarding bugs in BNO055 Euler Angle representation at high (>45deg) pitch/roll angles
        // Probably should switch to Quaternion mode and calculate Euler angles ourselves
    
       // Calibration status
        imuState.cal_accel = imu.readAccelCalibStatus();    //Accelerometer Calibration Status (0 - 3)
        imuState.cal_mag   = imu.readMagCalibStatus();      //Magnetometer Calibration Status (0 - 3)
        imuState.cal_gyro  = imu.readGyroCalibStatus();     //Gyroscope Calibration Status (0 - 3)
        imuState.cal_sys   = imu.readSystemCalibStatus();   //System Calibration Status (0 - 3)
    
        float ballpos_fast = ACCEL_GAIN * (ay_fast - ACCEL_ZERO);
        float ballpos_slow = ACCEL_GAIN * (ay_slow - ACCEL_ZERO);
    
        bool fBuzzFast = (abs(ballpos_fast) > BALL_THRESHOLD_FAST);
        bool fBuzzSlow = (abs(ballpos_slow) > BALL_THRESHOLD_SLOW);
    
        buzz_L = (ballpos_fast < 0) && (fBuzzFast || fBuzzSlow);
        buzz_R = (ballpos_fast > 0) && (fBuzzFast || fBuzzSlow);
    
        // heading calculations
        float  heading_filtered = imuState.heading;
  
        for (int ii = HEADING_BUF_LENGTH - 1; ii > 0; ii--)
          heading_buf[ii] = heading_buf[ii - 1];
        heading_buf[0] = heading_filtered;
        float heading_rate = heading_buf[0] - heading_buf[HEADING_BUF_LENGTH - 1];
        if (heading_rate <= -180)
          heading_rate += 360;
        if (heading_rate >= 180)
          heading_rate -= 360;
        heading_rate *= 1e6 / (TICK_PERIOD * HEADING_BUF_LENGTH);
    
        bool spin_R = (tick > HEADING_BUF_LENGTH) && (heading_rate > +SPIN_THRESHOLD);
        bool spin_L = (tick > HEADING_BUF_LENGTH) && (heading_rate < -SPIN_THRESHOLD);

#ifdef DEBUG_SPIN_LATCH
        spin_latch_R = spin_latch_R || spin_R;
        spin_latch_L = spin_latch_L || spin_L;

        spin_R = spin_latch_R;
        spin_L = spin_latch_L;
#endif        

        bool spin_mode = false;
        if (spin_R || spin_L)
          {        
            spin_counter++;
            if (spin_counter >= SPIN_MIN_TICKS)
              spin_mode = true;
          }
        else
          {
            spin_counter = 0;
          }
    
        bool startup_mode = (tick < STARTUP_BUZZ_TICKS);
    
        if (startup_mode)
          {
            bool buzz_startup = (((tick/1) % 2) == 1);
    
            motor_activate(motor_L, buzz_startup);
            motor_activate(motor_R, buzz_startup);
          }
        else if (spin_mode)
          {
            bool intermittent_state = ((tick % 2) == 0);
    
            motor_activate(motor_L, spin_R && intermittent_state);   // left rudder for right spin
            motor_activate(motor_R, spin_L && intermittent_state);   // right rudder for left spin
          }
        else
         // normal mode
          {
            motor_activate(motor_L, buzz_L);
            motor_activate(motor_R, buzz_R);
          }

#ifdef DEBUG_OUTPUT    
        Serial.print(tick);
        Serial.print(F(" "));
        Serial.print(ticktime);
        Serial.print(F(" "));
        Serial.print(ay_raw);
/*        
        Serial.print(F(" "));
        Serial.print(buzz_L ? "L" : " ");
        Serial.print(buzz_R ? "R" : " ");
        Serial.print(F(" A: "));
        Serial.print(ay_raw);
        Serial.print(F(" "));
        Serial.print(ballpos_fast);
        Serial.print(F(" M: "));
        Serial.print(heading_filtered);
        Serial.print(F(" "));
        Serial.print(heading_rate);
        Serial.print(F(" "));
        Serial.print(spin_L ? "sL" : "  ");
        Serial.print(spin_R ? "sR" : "  ");


/*
        Serial.print("--- rawA X/Y/Z: ");
        Serial.print(imuState.accelX);
        Serial.print(" ");
        Serial.print(imuState.accelY);
        Serial.print(" ");
        Serial.print(imuState.accelX);
    
        Serial.print(" H/P/R: ");
        Serial.print(imuState.heading);
        Serial.print(" ");
        Serial.print(imuState.pitch);
        Serial.print(" ");
        Serial.print(imuState.roll);
    
        Serial.print(" CalStatus=");
        Serial.print(imuState.cal_accel);
        Serial.print(imuState.cal_mag);
        Serial.print(imuState.cal_gyro);
        Serial.print(imuState.cal_sys);
*/
        Serial.println();
#endif

#ifdef DO_LOGGING  
        writeLog(tick, ticktime / 1000.0, imuState, ay_raw, ballpos_fast, ballpos_slow, buzz_L, buzz_R, heading_filtered, heading_rate, spin_L, spin_R);
#endif
    
        // flash LED to verify code is still running
        int flash_index = tick % 10;
        if ((flash_index == 0) || (flash_index == 3))
          digitalWrite(LED_BUILTIN, HIGH);
        else
          digitalWrite(LED_BUILTIN, LOW);
    
        tick++;
      }
  
    long time_sample_end = micros();
    long time_sample = time_sample_end - time_sample_start;
  
    if (time_sample < 0)
      Serial.println(F("Invalid sample time"));
  
    long delay_us = 0;
    if (time_sample < SAMPLE_PERIOD)
      delay_us = SAMPLE_PERIOD - time_sample;
  
    if (delay_us > 1000000)
      Serial.println(F("Invalid timing delay"));
  
    if (delay_us < 0)
      {
        Serial.println(F("Negative timing delay"));
        delay_us = 0;
      }
  
    delayMicroseconds(delay_us);
  }

// end
